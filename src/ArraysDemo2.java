

// 60

public class ArraysDemo2 {

	public static void main(String[] args) {

//		String str1 = "Hello";

//		Employee e = new Employee()

		String str2 = new String("Hello");

		String str3 = new String("Hello");

		if (str2 == str3) {
			System.out.println("String objects are equal by reference");
		} else {
			System.out.println("String objects are not equal by reference");
		}
		
		
// == => reference comparison
		if(str2.equals(str3)) {
			System.out.println("String objects are equal by content");
		}else {
			System.out.println("String objects are not equal by content");
		}
	}
}


