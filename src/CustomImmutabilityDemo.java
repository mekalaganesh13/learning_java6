

// new Employee("suresh");
// I want to add salutation(Mr/Miss) to my existing employee object;

//Once an object is created, you cannot make changes to the existing object.
//But if you want to do some changes on the existing object, 
//then with those changes a new object will be created for you.

class Employee {

	private String name;

	public Employee(String name) {
		this.name = name;
	}

	public Employee applySalutation(String salutation) {
		String fullName = salutation+this.name; // Mr.Suresh
		return new Employee(fullName);
	}
	
	public String getName() {
		return this.name;
	}
	
	
}

public class CustomImmutabilityDemo{
	
	public static void main(String[] args) {
		
		Employee employee = new Employee("Suresh"); 
		
		employee.applySalutation("Mr."); 
		
		System.out.println(employee.getName()); // Suresh
		
		Employee salutedEmployee = employee.applySalutation("Mr.");
		
		System.out.println(salutedEmployee.getName()); // Mr.Suresh
		
		
//		Suresh 1
//		Mr.Suresh 2
		
		
		
		
	}
}

